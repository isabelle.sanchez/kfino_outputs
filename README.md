# kfino 

@isabelle.sanchez & @bertrand.cloez - UMR MISTEA INRAE - 2020-2023 GPL-3

This repository provides outputs on real data of the **kfino** algorithm to detect impulse noised outliers: output_Arles_2021_kfino_graphs_EM_publi_juin.pdf.

The **kfino** algorithm was developed for time courses in order to detect impulse noised outliers and predict the parameter of interest mainly for data recorded on the walk-over-weighing system described in this publication:

E.González-García *et. al.* (2018) A mobile and automated walk-over-weighing system for a close and remote monitoring of liveweight in sheep. vol 153: 226-238. https://doi.org/10.1016/j.compag.2018.08.022

**Kalman filter with impulse noised outliers** (kfino) is a robust sequential algorithm allowing to filter data with a large number of outliers. This algorithm is based on simple latent linear Gaussian processes as in the Kalman Filter method and is devoted to detect impulse-noised outliers. These are data points that differ significantly from other observations.

The method is described in full details in the following arxiv preprint: https://arxiv.org/abs/2208.00961.

**kfino** is an R package available on CRAN and on gitlab:

* https://CRAN.R-project.org/package=kfino 
* https://forgemia.inra.fr/isabelle.sanchez/kfino
